<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista</title>
</head>
<body>
<h1>Añadir nombres:</h1>
<form method="post" action="?method=add">
        <label>Nombre:</label>
        <input type="text" name="nombre" value="">
        <br>
        <input type="submit" name="submit"><br>
        <hr>
        <h1>Lista de Nombres: </h1>
        <ul>
            <?php foreach ($nombres as $clave=> $nombre): ?>
                <li><?php echo $nombre ?></li>
                <input type="checkbox" name="nombres[]"
                value="<?php echo $nombre ?>"
                <?php echo in_array($nombre, $_SESSION['nombres']) ? 'checked' : '' ?>
                >
            <?php endforeach?>
        </ul>
        <a href="?method=delete=<?php echo $clave ?>">Borrar Nombres</a>
    </form>
</body>
</html>