package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ListaServlet
 */
@WebServlet("/lista")
public class ListaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ListaServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession sesion= request.getSession();
		String nombre=request.getParameter("nombre");
		String nombres=request.getParameter("nombres");
		sesion.setAttribute("nombre", nombre);
		sesion.setAttribute("nombres", nombres);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/lista.jsp");
		dispatcher.forward(request, response);
		return;
	}
	protected void add(HttpServletRequest request, HttpServletResponse response) throws IOException{
				String nombre=request.getParameter("nombre");
				String nombres=request.getParameter("nombres");
			response.sendRedirect(request.getContextPath() + "/add");

        }
		
	
	protected void delete(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession sesion= request.getSession();
		sesion.removeAttribute("nombre");
		response.sendRedirect(request.getContextPath() + "/lista");

	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher= request.getRequestDispatcher("/WEB-INF/lista.jsp");
		dispatcher.forward(request, response);	}

}
